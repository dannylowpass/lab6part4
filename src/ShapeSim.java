/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dannylowpass
 */
public class ShapeSim {
    
    public static void main(String[] args){
        
        Circle c1 = new Circle(8);
        Square s1 = new Square(6);
        
        System.out.println(c1.getArea(8));
        System.out.println(c1.getPerimeter(8));
        System.out.println(s1.getArea(6));
        System.out.println(s1.getPerimeter(6));
        
    }
    
}

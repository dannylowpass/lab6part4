/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dannylowpass
 */
public class Circle {
    
    double radius;
    
    public Circle(){
        
    }
    
    public Circle(double radius){
        this.radius = radius;
    }
    
    public double getArea(double radius){
        return radius * radius * 3.14156;
    }
    
    public double getPerimeter(double radius){
        return 2 * radius * 3.14156;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dannylowpass
 */
public class Square {
    
    double sideLength;
    
    public Square(){
        
    }
    
    public Square(double sideLength){
        this.sideLength = sideLength;
    }
    
    public double getArea(double length){
        return length * length;
    }
    
    public double getPerimeter(double length){
        return 4 * length;
    }
}

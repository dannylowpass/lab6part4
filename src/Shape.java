/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dannylowpass
 */
abstract class Shape {
    
    
    public abstract double getArea();
    
    public abstract double getPerimeter();
        
    
}
